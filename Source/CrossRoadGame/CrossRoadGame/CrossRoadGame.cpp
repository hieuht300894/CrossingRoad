// CrossRoadGame.cpp : Defines the entry point for the console application.
//

#include"stdafx.h"
#include<iostream>
#include<Windows.h>
using namespace std;

#pragma region CVEHICLE
class CVEHICLE {
protected:
	int mX, mY;
public:
	virtual void move(int, int);
};

void CVEHICLE::move(int, int)
{
}
#pragma endregion

#pragma region CANIMAL
class CANIMAL {
protected:
	int mX, mY;
public:
	virtual void move(int, int);
	virtual void tell();
};

void CANIMAL::move(int, int)
{
}

void CANIMAL::tell()
{
}
#pragma endregion

#pragma region CPEOPLE
class CPEOPLE {
protected:
	int mX, mY;
	bool mState; // State is live or dead
public:
	CPEOPLE();
	~CPEOPLE();
	void up(int);
	void down(int);
	void left(int);
	void right(int);
	bool isImpact(const CVEHICLE*&);
	bool isImpact(const CANIMAL*&);
	bool isFinish();
	bool isDead();
};

CPEOPLE::CPEOPLE()
{
}

CPEOPLE::~CPEOPLE()
{
}

void CPEOPLE::up(int key)
{
}

void CPEOPLE::down(int key)
{
}

void CPEOPLE::left(int key)
{
}

void CPEOPLE::right(int key)
{
}

bool CPEOPLE::isImpact(const CVEHICLE*& vehicles)
{
	return false;
}

bool CPEOPLE::isImpact(const CANIMAL*& animals)
{
	return false;
}

bool CPEOPLE::isFinish()
{
	return false;
}

bool CPEOPLE::isDead()
{
	return false;
}
#pragma endregion

#pragma region CTRUCK
class CTRUCK : public CVEHICLE {
};
#pragma endregion

#pragma region CCAR
class CCAR : public CVEHICLE {
};
#pragma endregion

#pragma region CDINAUSOR
class CDINAUSOR : public CANIMAL {
};
#pragma endregion

#pragma region CBIRD
class CBIRD : public CANIMAL {
};
#pragma endregion

#pragma region CGAME
class CGAME {
	CTRUCK* cTruck;
	CCAR* cCar;
	CDINAUSOR* cDinausor;
	CBIRD* cBird;
	CPEOPLE cPeople;
public:
	CGAME(); //Chuẩn bị dữ liệu cho tất cả các đối tượng
	~CGAME(); // Hủy tài nguyên đã cấp phát
	CPEOPLE getPeople();//Lấy thông tin người
	CVEHICLE* getVehicle();//Lấy danh sách các xe
	CANIMAL* getAnimal(); //Lấy danh sách các thú
	void drawGame(); //Thực hiện vẽ trò chơi ra màn hình sau khi có dữ liệu
	void resetGame(); // Thực hiện thiết lập lại toàn bộ dữ liệu như lúc đầu
	void exitGame(HANDLE); // Thực hiện thoát Thread
	void startGame(); // Thực hiện bắt đầu vào trò chơi
	void loadGame(istream); // Thực hiện tải lại trò chơi đã lưu
	void saveGame(istream); // Thực hiện lưu lại dữ liệu trò chơi
	void pauseGame(HANDLE); // Tạm dừng Thread
	void resumeGame(HANDLE); //Quay lai Thread
	void updatePosPeople(char); //Thực hiện điều khiển di chuyển của CPEOPLE
	void updatePosVehicle(); //Thực hiện cho CTRUCK & CCAR di chuyển
	void updatePosAnimal();//Thực hiện cho CDINAUSOR & CBIRD di chuyển
};

CGAME::CGAME() {}
CGAME::~CGAME() {}
CPEOPLE CGAME::getPeople() { return cPeople; }
CVEHICLE* CGAME::getVehicle() { return cCar; }
CANIMAL* CGAME::getAnimal() { return cBird; }
void CGAME::drawGame() {}
void CGAME::resetGame() {}
void CGAME::exitGame(HANDLE handle) {}
void CGAME::startGame() {}
void CGAME::loadGame(istream stream) {}
void CGAME::saveGame(istream stream) {}
void CGAME::pauseGame(HANDLE handle) {}
void CGAME::resumeGame(HANDLE handle) {}
void CGAME::updatePosPeople(char key) {}
void CGAME::updatePosVehicle() {}
void CGAME::updatePosAnimal() {}
#pragma endregion



#pragma region CCONSOLE
class CCONSOLE {
public:
	void ResizeConsole(int, int);
	bool GetBufferInfo(int &, int &);
	void ChangeColor(int);
	void GotoXY(int, int);
	void ClearScreen();
};

void CCONSOLE::ResizeConsole(int width, int height)
{
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r);
	MoveWindow(console, r.left, r.top, width, height, TRUE);
}

// Get buffer infomation of screen
bool CCONSOLE::GetBufferInfo(int &bufferWidth, int &bufferHeight) {
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO bufferInfo;

	if (GetConsoleScreenBufferInfo(handle, &bufferInfo))
	{
		bufferWidth = bufferInfo.srWindow.Right - bufferInfo.srWindow.Left + 1;
		bufferHeight = bufferInfo.srWindow.Bottom - bufferInfo.srWindow.Top + 1;
		return true;
	}
	return false;
}

// Change text color
void CCONSOLE::ChangeColor(int x)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(handle, x);
}

// Move cursor to point
void CCONSOLE::GotoXY(int x, int y)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD position;
	position.X = x;
	position.Y = y;
	SetConsoleCursorPosition(handle, position);
}

// Clear screen
void CCONSOLE::ClearScreen() {
	system("cls");
}
#pragma endregion

int main()
{
	CCONSOLE console;
	console.ResizeConsole(800, 400);

	int width = 0;
	int height = 0;
	console.GetBufferInfo(width, height);

	int* array = new int[width];
	for (int i = 0; i < width; i++)
	{
		if (i % 2 == 0)
			array[i] = 0;
		else
			array[i] = 1;
	}

	int i = 0;
	int j = 0;
	while (i < width)
	{
		console.ClearScreen();
		j = 0;

		while (j < width)
		{
			console.GotoXY(j, 0);

			if (array[j] == 1)
			{
				cout << 'A';
				array[j] = 0;
			}
			else
				array[j] = 1;

			j++;

			Sleep(1);
		}

		i++;
		Sleep(1000);
	}

	delete array;

	system("pause");
	return 0;
}